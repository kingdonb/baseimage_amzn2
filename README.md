### Docker baseimage for Amazon Linux 2

Based loosely on:
https://github.com/pokle/centos-baseimage
https://rhatdan.wordpress.com/2014/04/30/running-systemd-within-a-docker-container/

Built against the latest Amazon Linux 2 release tag:

- amazonlinux:2.0.20190115

You can run your containers with a systemd init, if they might need to support
multiple services, or are directly replacing a production system with multiple
intertwined services that cannot easily be separated.

```Dockerfile
FROM amazonlinux:2.0.20190115
MAINTAINER "Kingdon Barrett" <kingdon@teamhephy.com>

...
```

Right now, it just installs and runs systemd.
