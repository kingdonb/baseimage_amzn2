FROM amazonlinux:2.0.20190115

# Based on https://rhatdan.wordpress.com/2014/04/30/running-systemd-within-a-docker-container/
# Also adapted from https://github.com/pokle/centos-baseimage/
MAINTAINER "Kingdon Barrett" <kingdon@teamhephy.com>
ENV container docker

RUN mkdir /build

# No EPEL in Amazon
#ADD build/epel /build/epel
#RUN /build/epel/install.sh

# SystemD
ADD build/systemd /build/systemd
RUN /build/systemd/install.sh

# No runit, we will use systemd
#ADD build/runit /build/runit
#RUN /build/runit/install.sh

# No SSHD for now
#ADD build/sshd /build/sshd
#RUN /build/sshd/install.sh

# No rsyslogd, we will use journald
#ADD build/rsyslogd /build/rsyslogd
#RUN /build/rsyslogd/install.sh

ADD build/clean.sh /build/clean.sh
RUN /build/clean.sh

VOLUME ["/sys/fs/cgroup"]

#CMD runit
CMD ["/usr/sbin/init"]
